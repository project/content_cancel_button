<?php

/**
 * @file
 * Provides a cancel button on node forms.
 */

/**
 * Implements hook_help().
 */
function content_cancel_button_help($path, $arg) {
  switch ($path) {
    case 'admin/help#content_cancel_button':
      $output = '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('The Content cancel button module provides a cancel button on node forms.') . '</p>';
      return $output;
  }
}

/**
 * Custom cancel button callback.
 */
function content_cancel_button_form_cancel($form, &$form_state) {
  $fallback_destinaton = '<front>';
  // If edit, use the node itself as fallback.
  if (isset($form['#node']) && !empty($form['#node']->nid)) {
    $fallback_destinaton = 'node/' . $form['#node']->nid;
  }
  // Go to destination or fallback.
  $url = isset($_GET['destination']) ? $_GET['destination'] : $fallback_destinaton;
  drupal_goto($url);
}

/**
 * Implements hook_form_alter().
 */
function content_cancel_button_form_alter(&$form, &$form_state, $form_id) {
  if (stristr($form_id, '_node_form') !== FALSE) {
    $form['actions']['cancel'] = array(
      '#type' => 'submit',
      '#value' => t('Cancel'),
      '#access' => TRUE,
      '#weight' => 999,
      '#submit' => array('content_cancel_button_form_cancel', 'node_form_submit_build_node'),
      '#limit_validation_errors' => array(),
      '#attributes' => array('class' => array('btn-cancel')),
    );
  }
}
